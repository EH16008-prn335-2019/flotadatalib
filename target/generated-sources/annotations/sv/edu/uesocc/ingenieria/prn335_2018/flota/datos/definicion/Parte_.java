package sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.ModeloParte;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.SubTipoParte;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-11-10T06:59:30")
@StaticMetamodel(Parte.class)
public class Parte_ { 

    public static volatile SingularAttribute<Parte, SubTipoParte> idSubTipoParte;
    public static volatile ListAttribute<Parte, ModeloParte> modeloParteList;
    public static volatile SingularAttribute<Parte, String> observaciones;
    public static volatile SingularAttribute<Parte, String> nombre;
    public static volatile SingularAttribute<Parte, Integer> idParte;
    public static volatile SingularAttribute<Parte, Boolean> activo;

}