package sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Reserva;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-11-10T06:59:30")
@StaticMetamodel(TipoUsuario.class)
public class TipoUsuario_ { 

    public static volatile SingularAttribute<TipoUsuario, Integer> idTipoUsuario;
    public static volatile ListAttribute<TipoUsuario, Reserva> reservaList;
    public static volatile SingularAttribute<TipoUsuario, String> appId;
    public static volatile SingularAttribute<TipoUsuario, String> observaciones;
    public static volatile SingularAttribute<TipoUsuario, String> nombre;
    public static volatile SingularAttribute<TipoUsuario, Boolean> activo;

}