package sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Modelo;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Parte;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-11-10T06:59:30")
@StaticMetamodel(ModeloParte.class)
public class ModeloParte_ { 

    public static volatile SingularAttribute<ModeloParte, Long> idModeloParte;
    public static volatile SingularAttribute<ModeloParte, String> observaciones;
    public static volatile SingularAttribute<ModeloParte, Modelo> idModelo;
    public static volatile SingularAttribute<ModeloParte, Boolean> activo;
    public static volatile SingularAttribute<ModeloParte, Parte> idParte;

}