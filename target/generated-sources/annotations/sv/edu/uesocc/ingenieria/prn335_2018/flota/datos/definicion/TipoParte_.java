package sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.SubTipoParte;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-11-10T06:59:30")
@StaticMetamodel(TipoParte.class)
public class TipoParte_ { 

    public static volatile SingularAttribute<TipoParte, Integer> idTipoParte;
    public static volatile SingularAttribute<TipoParte, String> observaciones;
    public static volatile ListAttribute<TipoParte, SubTipoParte> subTipoParteList;
    public static volatile SingularAttribute<TipoParte, String> nombre;
    public static volatile SingularAttribute<TipoParte, Boolean> activo;

}